package main

import (
	"cenogen/datastruct"
	"context"
	"fmt"
)

func (r *inventoryRepository) GetInventoryReservedJSONByID(
	ctx context.Context,
	idList []string,
) ([]datastruct.Inventory, error) {
	rows, err := r.db.Query(ctx, `
		SELECT
		    product_id, 
		    json_field -> 'reserved' 
		FROM
		    inventory_json
		WHERE
		    product_id = ANY($1)`,
		idList)
	if err != nil {
		return nil, fmt.Errorf("error querying inventory: %w", err)
	}
	defer rows.Close()

	var inventoryList []datastruct.Inventory
	for rows.Next() {
		var inventory datastruct.Inventory
		err = rows.Scan(
			&inventory.ProductID,
			&inventory.Reserved,
		)
		if err != nil {
			return nil, fmt.Errorf("error scanning inventory: %w", err)
		}

		inventoryList = append(inventoryList, inventory)
	}

	return inventoryList, nil
}
