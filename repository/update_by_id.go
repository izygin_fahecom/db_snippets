package main

import (
	"cenogen/datastruct"
	"context"
	"fmt"
	"github.com/go-kratos/kratos/v2/log"
)

const updateInventoryStatement = "updateInventoryStatement"

func (r *inventoryRepository) UpdateInventory(
	ctx context.Context,
	inventoryList []datastruct.Inventory,
) error {
	conn, err := r.db.Acquire(ctx)
	if err != nil {
		return fmt.Errorf("error acquiring connection: %w", err)
	}
	defer conn.Release()
	statement, err := conn.Conn().Prepare(ctx, updateInventoryStatement, `
			UPDATE inventory SET
                location_code=$1,
                quantity=$2,
                reserved=$3,
                updated_at=NOW()
            WHERE
                product_id=$4
		`)
	if err != nil {
		return fmt.Errorf("error preparing statement: %w", err)
	}

	for _, inventory := range inventoryList {
		_, err := conn.Conn().Exec(ctx, statement.Name,
			inventory.LocationCode,
			inventory.Quantity,
			inventory.Reserved,
			inventory.ProductID,
		)
		if err != nil {
			return fmt.Errorf("error updating inventory: %w", err)
		}
	}

	err = conn.Conn().Deallocate(context.Background(), statement.Name)
	if err != nil {
		log.Errorw("msg", "conn.Deallocate failed", "err", err)
	}

	return nil
}
