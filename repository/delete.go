package main

import (
	"context"
	"fmt"
)

func (r *inventoryRepository) DeleteInventoryByID(
	ctx context.Context,
	idList []string,
) error {
	// delete by chunks of 100
	for i := 0; i < len(idList); i += 100 {
		end := i + 100
		if end > len(idList) {
			end = len(idList)
		}
		_, err := r.db.Exec(ctx, `
			DELETE FROM
			    inventory
			WHERE
			    product_id = ANY($1)`,
			idList[i:end])
		if err != nil {
			return fmt.Errorf("error deleting inventory: %w", err)
		}
	}

	return nil
}
