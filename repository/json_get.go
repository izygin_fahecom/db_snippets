package main

import (
	"cenogen/datastruct"
	"context"
	"encoding/json"
	"fmt"
)

type InventoryJSONData struct {
	LocationCode string `json:"location_code"`
	Quantity     int    `json:"quantity"`
	Reserved     int    `json:"reserved"`
}

func (r *inventoryRepository) GetInventoryJSONByID(
	ctx context.Context,
	idList []string,
) ([]datastruct.Inventory, error) {
	var inventoryList []datastruct.Inventory
	rows, err := r.db.Query(ctx, `
		SELECT
		    product_id,
		    json_field
		FROM
		    inventory_json
		WHERE
		    product_id = ANY($1)`,
		idList)
	if err != nil {
		return nil, fmt.Errorf("error querying inventory: %w", err)
	}
	defer rows.Close()
	for rows.Next() {
		var inventory datastruct.Inventory
		var jsonData InventoryJSONData
		var rawJSON []byte
		err = rows.Scan(
			&inventory.ProductID,
			&rawJSON,
		)
		if err != nil {
			return nil, fmt.Errorf("error scanning inventory: %w", err)
		}
		err = json.Unmarshal(rawJSON, &jsonData)
		if err != nil {
			return nil, fmt.Errorf("error unmarshalling inventory: %w", err)
		}
		inventory.LocationCode = jsonData.LocationCode
		inventory.Quantity = jsonData.Quantity
		inventory.Reserved = jsonData.Reserved

		inventoryList = append(inventoryList, inventory)
	}

	return inventoryList, nil
}
