package main

import (
	"cenogen/datastruct"
	"context"
	"fmt"
	"github.com/go-kratos/kratos/v2/log"
)

const updateInventoryStatementJSON = "updateInventoryStatementJSON"

func (r *inventoryRepository) UpdateInventoryJSON(
	ctx context.Context,
	inventoryList []datastruct.Inventory,
) ([]datastruct.Inventory, error) {
	conn, err := r.db.Acquire(ctx)
	if err != nil {
		return nil, fmt.Errorf("error acquiring connection: %w", err)
	}
	defer conn.Release()

	statement, err := conn.Conn().Prepare(ctx, insertInventoryStatementJSON, `
			UPDATE inventory_json
			SET
			    json_field=jsonb_set(json_field, '{reserved}', $2)
			WHERE
			    product_id=$1
		`)
	if err != nil {
		return nil, fmt.Errorf("error preparing statement: %w", err)
	}
	result := make([]datastruct.Inventory, len(inventoryList))
	for _, inventory := range inventoryList {
		_, err := conn.Conn().Exec(
			ctx,
			statement.Name,
			inventory.ProductID,
			inventory.Reserved,
		)
		if err != nil {
			return nil, fmt.Errorf("error updating inventory: %w", err)
		}
		result = append(result, inventory)
	}

	err = conn.Conn().Deallocate(context.Background(), statement.Name)
	if err != nil {
		log.Errorw("msg", "conn.Deallocate failed", "err", err)
	}

	return result, nil
}
