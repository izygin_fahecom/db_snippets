package main

import (
	"cenogen/datastruct"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-kratos/kratos/v2/log"
)

const insertInventoryStatementJSON = "insertInventoryStatementJSON"

func (r *inventoryRepository) InsertInventoryJSON(
	ctx context.Context,
	inventoryList []datastruct.Inventory,
) ([]datastruct.Inventory, error) {
	conn, err := r.db.Acquire(ctx)
	if err != nil {
		return nil, fmt.Errorf("error acquiring connection: %w", err)
	}
	defer conn.Release()

	statement, err := conn.Conn().Prepare(ctx, insertInventoryStatementJSON, `
			INSERT INTO inventory_json
			(
			    json_field
			) VALUES (
                $1
			)
			RETURNING
			    product_id
		`)
	if err != nil {
		return nil, fmt.Errorf("error preparing statement: %w", err)
	}
	result := make([]datastruct.Inventory, len(inventoryList))
	for _, inventory := range inventoryList {
		var jsonField InventoryJSONData
		jsonField.LocationCode = inventory.LocationCode
		jsonField.Quantity = inventory.Quantity
		jsonField.Reserved = inventory.Reserved
		data, err := json.Marshal(jsonField)
		if err != nil {
			return nil, fmt.Errorf("error marshaling json: %w", err)
		}
		row := conn.Conn().QueryRow(
			ctx,
			statement.Name,
			data,
		)
		err = row.Scan(&inventory.ProductID)
		if err != nil {
			return nil, fmt.Errorf("error inserting inventory: %w", err)
		}
		result = append(result, inventory)
	}

	err = conn.Conn().Deallocate(context.Background(), statement.Name)
	if err != nil {
		log.Errorw("msg", "conn.Deallocate failed", "err", err)
	}

	return result, nil
}
