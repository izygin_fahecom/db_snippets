package main

import (
	"context"
	"fmt"
)

func (r *inventoryRepository) IncreaseInventory(
	ctx context.Context,
	idList []string,
	increaseBy int,
) error {
	// delete by chunks of 100
	for i := 0; i < len(idList); i += 100 {
		end := i + 100
		if end > len(idList) {
			end = len(idList)
		}
		_, err := r.db.Exec(ctx, `
			UPDATE
			    inventory
			SET
			    quantity = quantity + $2
			WHERE
			    product_id = ANY($1)`,
			idList[i:end],
			increaseBy,
		)
		if err != nil {
			return fmt.Errorf("error deleting inventory: %w", err)
		}
	}
	return nil
}
