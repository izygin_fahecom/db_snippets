package main

import (
	"cenogen/datastruct"
	"context"
	"fmt"
)

func (r *inventoryRepository) GetInventoryByID(
	ctx context.Context,
	idList []int,
) ([]datastruct.Inventory, error) {
	var inventoryList []datastruct.Inventory
	rows, err := r.db.Query(ctx, `
		SELECT
		    product_id,
		    location_code,
		    quantity,
		    reserved
		FROM
		    inventory
		WHERE
		    product_id = ANY($1)`,
		idList)
	if err != nil {
		return nil, fmt.Errorf("error querying inventory: %w", err)
	}
	defer rows.Close()
	for rows.Next() {
		var inventory datastruct.Inventory
		err = rows.Scan(
			&inventory.ProductID,
			&inventory.LocationCode,
			&inventory.Quantity,
			&inventory.Reserved,
		)
		if err != nil {
			return nil, fmt.Errorf("error scanning inventory: %w", err)
		}
		inventoryList = append(inventoryList, inventory)
	}

	return inventoryList, nil
}
