package main

import (
	"cenogen/datastruct"
	"context"
	"fmt"
	"github.com/go-kratos/kratos/v2/log"
)

const upsertInventoryStatement = "upsertInventoryStatement"

func (r *inventoryRepository) UpsertInventory(
	ctx context.Context,
	inventoryList []datastruct.Inventory,
) ([]datastruct.Inventory, error) {
	conn, err := r.db.Acquire(ctx)
	if err != nil {
		return nil, fmt.Errorf("error acquiring connection: %w", err)
	}
	defer conn.Release()

	statement, err := conn.Conn().Prepare(ctx, upsertInventoryStatement, `
			INSERT INTO inventory
			(
			    location_code,
                quantity,
                reserved
			) VALUES (
			    $1,
                $2,
                $3
			)
			ON CONFLICT (location_code)
			DO UPDATE SET
				quantity=EXCLUDED.quantity,
				reserved=EXCLUDED.reserved
			WHERE quantity IS DISTINCT FROM EXCLUDED.quantity OR
			reserved IS DISTINCT FROM EXCLUDED.reserved
			RETURNING
			    product_id
		`)
	if err != nil {
		return nil, fmt.Errorf("error preparing statement: %w", err)
	}
	result := make([]datastruct.Inventory, len(inventoryList))
	for _, inventory := range inventoryList {
		row := conn.Conn().QueryRow(ctx, statement.Name,
			inventory.LocationCode,
			inventory.Quantity,
			inventory.Reserved,
		)
		err = row.Scan(&inventory.ProductID)
		if err != nil {
			return nil, fmt.Errorf("error inserting inventory: %w", err)
		}
		result = append(result, inventory)
	}

	err = conn.Conn().Deallocate(context.Background(), statement.Name)
	if err != nil {
		log.Errorw("msg", "conn.Deallocate failed", "err", err)
	}

	return result, nil
}
