package main

import (
	"github.com/jackc/pgx/v5/pgxpool"
)

type inventoryRepository struct {
	db *pgxpool.Pool
}

// nolint: revive
func NewInventoryRepository(db *pgxpool.Pool) *inventoryRepository {
	return &inventoryRepository{
		db: db,
	}
}
