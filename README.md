# PostgreSQL Snippets

Very basic code snippets to reference to



## Getting started

#### Driver
Driver: https://github.com/jackc/pgx  
Pooler: github.com/jackc/pgx/v5/pgxpool

#### Migrations

1. Migrations library:  
https://github.com/pressly/goose
2. [Migrations folder](migrations/)
3. [Migrations executing code](migrate/main.go)


#### Snippets

1. [Initialization](main.go)
2. [Get by ID](repository/get_by_id.go)
3. [Insert list](repository/insert.go)
4. [Upsert list](repository/upsert.go)
5. [Update list](repository/update_by_id.go)
6. [Delete batch](repository/delete.go)
7. [Increment (atomic increase/descrease)](repository/increment_by_id.go)


