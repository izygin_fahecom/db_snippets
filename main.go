package main

import (
	"cenogen/datastruct"
	"context"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
)

func generateConnectionURL(username string, password string, database string, host string, port int, poolMaxConns int) string {
	return fmt.Sprintf("postgres://%s:%s@%s:%d/%s?pool_max_conns=%d", username, password, host, port, database, poolMaxConns)
}

func main() {
	username := "igor"
	password := "password"
	database := "inventory"
	host := "localhost"
	port := 5432
	poolMaxConns := 10

	//"postgres://igor:@localhost:5432/inventory?pool_max_conns=10"
	pgxPool, err := pgxpool.New(context.Background(), generateConnectionURL(username, password, database, host, port, poolMaxConns))
	if err != nil {
		panic(err)
	}
	repository := NewInventoryRepository(pgxPool)
	insert(repository)
}

func insert(repository *inventoryRepository) {
	_, err := repository.InsertInventoryJSON(context.Background(), []datastruct.Inventory{
		{
			LocationCode: "location_code",
			Quantity:     1,
			Reserved:     1,
		},
	})
	if err != nil {
		panic(err)
	}
}
