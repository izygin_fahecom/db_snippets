package datastruct

import "time"

type Inventory struct {
	ProductID    string
	LocationCode string
	Quantity     int
	Reserved     int
	UpdatedAt    time.Time
}

type SetInventory struct {
	ProductID    string
	LocationCode string
	Quantity     int
}
