-- +goose Up

CREATE TABLE inventory
(
    product_id BIGSERIAL PRIMARY KEY,
    quantity BIGINT NOT NULL,
    reserved BIGINT NOT NULL,
    location_code TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
);


CREATE TABLE inventory_json
(
    product_id BIGSERIAL PRIMARY KEY,
    json_field JSONB,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
);

